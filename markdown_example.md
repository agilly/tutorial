#### A Cautionary Tale
by _**Dr. John Hammurabi**_, University of Niniveh.
##### Part One : *Awakening*
The ~~young boy~~ child had been walking in the desert for two days when he spotted the carved stone. He looked at it carefully. He was looking for directions, for he was lost. But he did not recognise the markings. In fact, the only thing that he could see was this table, deeply etched into the rock:

|$-\pi$|$-\frac{5\pi}{6}$|$-\frac{3\pi}{4}$|$-\frac{2\pi}{3}$|$-\frac{\pi}{2}$|$?$|$-\frac{\pi}{4}$|$-\frac{\pi}{6}$|$0$|
|----|----|----|------|-----|------|
|$0$|$-\frac{1}{2}$|$-\frac{\sqrt{2}}{2}$|$-\frac{\sqrt{3}}{2}$|$1$|$-\frac{\sqrt{3}}{2}$|$-\frac{\sqrt{2}}{2}$|$-\frac{1}{2}$|$0$|
|$-1$|$-\frac{\sqrt{3}}{2}$|$-\frac{\sqrt{2}}{2}$|$-\frac{1}{2}$|$0$|$\frac{1}{2}$|$\frac{\sqrt{2}}{2}$|$\frac{\sqrt{3}}{2}$|$1$
<br><br>
The boy despaired, for he was feeling the following:
* anguish
 * despair
 * fear
* hunger
* thirst
<br><br>
Or rather, in order of importance:
1. thirst
2. anguish
3. hunger
<br><br>
In fact, what the boy didn't know was that the stone was telling him precisely where to go. He did not know trigonometry, because he had made fun of his teacher and played dice in class instead of studying. Terefore he wandered in the desert for seven more days until he finally died. If he had known trigonometry, he would have come home to his mother who would have welcomed him with tears of joy. 
> _Don't end like this boy, read about trigonometry on [Wikipedia](https://en.wikipedia.org/wiki/Trigonometry)\!_
##### Part Two: Appendix
If the boy had known `python`, he might have found the solution this way:
```python
import math
print "acos(1/2)/pi : ",  math.acos(0.5)/math.pi
```
As simple as _that_!
